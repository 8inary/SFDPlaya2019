# SFDPlaya2019

*Repositorio de info/imágenes/archivos sobre el Software Freedom Day Playa del Carmen 2019 .*


**Software Freedom Day Playa del Carmen 2019**

El [**Software Freedom Day**](https://www.softwarefreedomday.org/) (**SFD**) es un evento que se celebra cada año, generalmente cada 3er Sábado de Septiembre, donde se pretende festejar al Software Libre 
por medio de pláticas, talleres, comida, etc . El SFD se festeja desde 2005 y es un día a nivel internacional. 

En **Playa del Carmen** lo festejaremos el **28 de Septiembre** de este año 2019 en el **Salón Oval del Nuevo Palacio Municipal** en horario de *9:30 a 16:00*. 
Habra café, comida, regalos y sobre todo difusión de Software Libre para los asistentes.

Tambien para este año habra un torneo de [SuperTuxKart](https://supertuxkart.net/Main_Page) ; puedes decargarlo desde su página oficial, 
jugarlo y asi practicar para el torneo; el juego es [Software Libre](https://www.gnu.org/philosophy/free-sw.es.html) y lo lo puedes descargar
e instalar libremente en Android, Windows, MacOS y GNU/Linux. Si quieres donar al creador/desarrollador del juego puedes hacerlo en su página oficial .

Si piensas asistir al evento, existe un [código de conducta](https://gitlab.com/8inary/SFDPlaya2019/blob/master/CodigodeConductaSFD.pdf) que debes 
leer y acatar.

Más información, envia un correo a sfdplaya@pm.me y en el grupo público de [Telegram](https://t.me/sfdpdc) .